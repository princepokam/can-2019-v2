package stadiums;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rouftom.can2019.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StadiumFragmentSub extends Fragment {

    public StadiumFragmentSub() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentHensSubLayout = inflater.inflate(R.layout.fragment_stadiums_sub, container, false);
        // Inflate the layout for this fragment
        return fragmentHensSubLayout;
    }


}
