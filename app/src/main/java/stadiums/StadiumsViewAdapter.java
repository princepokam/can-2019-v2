package stadiums;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class StadiumsViewAdapter extends FragmentPagerAdapter {
    // tab titles
    private String[] tabTitles = new String[]{"Stade 1", "Stade 2", "Stade 3", "Stade 4", "Stade 5"};

    public StadiumsViewAdapter(FragmentManager fm) {
        super(fm);
    }

    // overriding getPageTitle()
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                StadiumFragmentSub stadiumFragmentSub = new StadiumFragmentSub();
                return stadiumFragmentSub;

            case 1:
                StadiumFragmentSub stadiumFragmentSub1 = new StadiumFragmentSub();
                return stadiumFragmentSub1;

            case 2:
                StadiumFragmentSub stadiumFragmentSub2 = new StadiumFragmentSub();
                return stadiumFragmentSub2;

            case 3:
                StadiumFragmentSub stadiumFragmentSub3 = new StadiumFragmentSub();
                return stadiumFragmentSub3;

            case 4:
                StadiumFragmentSub stadiumFragmentSub4 = new StadiumFragmentSub();
                return stadiumFragmentSub4;

                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}
