package com.example.rouftom.can2019;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hens.HensViewAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class HensFragment extends Fragment {
    private TextView sentLabel;
    private TextView receivedLabel;
    private ViewPager trViewPager;
    private HensViewAdapter mHensViewAdapter;
    FragmentActivity fragmentBelongActivity;
    FragmentManager fragmentManager;
    TabLayout tabLayout;

    public HensFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View retView = inflater.inflate(R.layout.fragment_hens, container, false);

        initComponents(retView);
        handleEvents();

        return retView;

    }


    private void initComponents(View retView) {
        fragmentBelongActivity = (FragmentActivity) getActivity();
        fragmentBelongActivity.setTitle(R.string.poules);
        fragmentManager = getChildFragmentManager();

        tabLayout = (TabLayout) retView.findViewById(R.id.tab_layout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (2 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#ffffff"));

        trViewPager = (ViewPager) retView.findViewById(R.id.transactionsPager);
        mHensViewAdapter = new HensViewAdapter(fragmentManager);
        trViewPager.setAdapter(mHensViewAdapter);
        tabLayout.setupWithViewPager(trViewPager);
    }

    private void handleEvents() {
        trViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()     {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        trViewPager.setCurrentItem(0);
                        break;
                    case 1:
                        trViewPager.setCurrentItem(1);
                        break;
                    case 2:
                        trViewPager.setCurrentItem(2);
                        break;
                    case 3:
                        trViewPager.setCurrentItem(3);
                        break;
                    case 4:
                        trViewPager.setCurrentItem(4);
                        break;
                    case 5:
                        trViewPager.setCurrentItem(5);
                        break;
                    case 6:
                        trViewPager.setCurrentItem(6);
                        break;
                    case 7:
                        trViewPager.setCurrentItem(7);
                        break;
                    default:
                        trViewPager.setCurrentItem(tab.getPosition());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
