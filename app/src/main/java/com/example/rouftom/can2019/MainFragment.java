package com.example.rouftom.can2019;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class MainFragment extends Fragment {
    FragmentActivity fragmentBelongActivity;
    FragmentManager fragmentManager;

    public MainFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View retView = inflater.inflate(R.layout.fragment_main, container, false);

        initComponents(retView);
        handleEvents();

        return retView;

    }


    private void initComponents(View retView) {
        fragmentBelongActivity = (FragmentActivity) getActivity();
        fragmentBelongActivity.setTitle(R.string.app_name);
        fragmentManager = getChildFragmentManager();
    }

    private void handleEvents() {

    }

}
