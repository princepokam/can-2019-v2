package com.example.rouftom.can2019;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    FragmentManager ft;
    private static AlertDialog customDialog;
    Date dateEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ft = getSupportFragmentManager();
        ft.beginTransaction().replace(R.id.flMain, new MainFragment()).commit();

        try {
            dateEvent = new SimpleDateFormat("yyyy-MM-dd").parse("2019-06-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.compte_rebours) {
            showCustomDialog();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_teams) {
            // Handle the camera action
            ft.beginTransaction().replace(R.id.flMain, new TeamsFragment()).commit();

        } else if (id == R.id.nav_hens) {
            ft.beginTransaction().replace(R.id.flMain, new HensFragment()).commit();

        } else if (id == R.id.nav_qualifications) {
            ft.beginTransaction().replace(R.id.flMain, new QualificationsFragment()).commit();

        } else if (id == R.id.nav_stadiums) {
            ft.beginTransaction().replace(R.id.flMain, new StadiumsFragment()).commit();

        } else if (id == R.id.nav_matchs) {
            ft.beginTransaction().replace(R.id.flMain, new MatchsFragment()).commit();

        } else if (id == R.id.nav_hotels) {
            ft.beginTransaction().replace(R.id.flMain, new HotelsFragment()).commit();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    void showCustomDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_compte_rebours, null);
        final TextView tvCompteRebours = (TextView) mView.findViewById(R.id.tv_compte_rebours);

        mBuilder.setView(mView);
        customDialog = mBuilder.create();
        customDialog.setCancelable(true);
        customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                customDialog = null;
            }
        });
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();

        Thread splashThread = new Thread() {
            String remainTime = "";
            @Override
            public void run() {
                try {
                    while (dateEvent.compareTo(new Date()) > 0 ) {
                        long diff = dateEvent.getTime() - new Date().getTime();
                        int numOfDays = (int)(diff / 1000 / 60 / 60 / 24);
                        Date d = new Date(dateEvent.getTime() - new Date().getTime());
                        remainTime = numOfDays+" JJ "+ d.getHours()+" HH "+d.getMinutes()+ " MM "+d.getSeconds()+" SS";

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvCompteRebours.setText(remainTime);
                            }
                        });
                        sleep(1000);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        splashThread.start();
    }

}
