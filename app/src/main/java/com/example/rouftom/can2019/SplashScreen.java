package com.example.rouftom.can2019;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SplashScreen extends Activity {

    Thread splashThread;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        starAnimations();
    }

    private void starAnimations() {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        animation.reset();
        LinearLayout l = (LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(animation);

        animation = AnimationUtils.loadAnimation(this, R.anim.translate_logo);
        animation.reset();
        TextView i = (TextView) findViewById(R.id.splashtitle);
        i.clearAnimation();
        i.startAnimation(animation);

        animation = AnimationUtils.loadAnimation(this, R.anim.translate);
        animation.reset();
        TextView t = (TextView) findViewById(R.id.splashtitle);
        t.clearAnimation();
        t.startAnimation(animation);

        splashThread = new Thread() {
            @Override
            public void run() {
                try {

                    int waited = 0;
                    // Splash Screen pause
                    while (waited < 3500) {
                        sleep(100);
                        waited += 100;
                    }
                    Intent it = new Intent(SplashScreen.this, MainActivity.class);
                    it.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(it);
                    SplashScreen.this.finish();

                } catch (InterruptedException e) {

                } finally {
                    SplashScreen.this.finish();
                }
            }
        };
        splashThread.start();
    }


}
