package com.example.rouftom.can2019;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hens.HensViewAdapter;
import stadiums.StadiumsViewAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class StadiumsFragment extends Fragment {
    private ViewPager stadiumsViewPager;
    private StadiumsViewAdapter mStadiumsViewAdapter;
    FragmentActivity fragmentBelongActivity;
    FragmentManager fragmentManager;
    TabLayout tabLayout;



    public StadiumsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View retView = inflater.inflate(R.layout.fragment_stadiums, container, false);

        initComponents(retView);
        handleEvents();
        
        return retView;
    }



    private void initComponents(View retView) {
        fragmentBelongActivity = (FragmentActivity) getActivity();
        fragmentBelongActivity.setTitle(R.string.stades);
        fragmentManager = getChildFragmentManager();

        tabLayout = (TabLayout) retView.findViewById(R.id.stadium_tab_layout);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));
        tabLayout.setSelectedTabIndicatorHeight((int) (2 * getResources().getDisplayMetrics().density));
        tabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#ffffff"));

        stadiumsViewPager = (ViewPager) retView.findViewById(R.id.stadiumsPager);
        mStadiumsViewAdapter = new StadiumsViewAdapter(fragmentManager);
        stadiumsViewPager.setAdapter(mStadiumsViewAdapter);
        tabLayout.setupWithViewPager(stadiumsViewPager);
    }

    private void handleEvents() {
        stadiumsViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener()     {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        stadiumsViewPager.setCurrentItem(0);
                        break;
                    case 1:
                        stadiumsViewPager.setCurrentItem(1);
                        break;
                    case 2:
                        stadiumsViewPager.setCurrentItem(2);
                        break;
                    case 3:
                        stadiumsViewPager.setCurrentItem(3);
                        break;
                    case 4:
                        stadiumsViewPager.setCurrentItem(4);
                        break;
                    default:
                        stadiumsViewPager.setCurrentItem(tab.getPosition());
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}
