package com.example.rouftom.can2019;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import lib.LinearLayoutManagerWithSmoothScroller;
import teams.TeamsAdapter;
import teams.TeamsData;
import teams.TeamsDataModel;


/**
 * A simple {@link Fragment} subclass.
 */
public class TeamsFragment extends Fragment {
    FragmentActivity fragmentBelongActivity;
    FragmentManager fragmentManager;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<TeamsDataModel> data;


    public TeamsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentBelongActivity = (FragmentActivity) getActivity();
        fragmentBelongActivity.setTitle(R.string.equipes);
        fragmentManager = getChildFragmentManager();

        View fragmentTeamsSubLayout = inflater.inflate(R.layout.fragment_teams, container, false);
        recyclerView = (RecyclerView) fragmentTeamsSubLayout.findViewById(R.id.teams_list);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(getContext()));


        data = new ArrayList<TeamsDataModel>();
        for (int i = 0; i < TeamsData.numberArray.length; i++) {
            data.add(new TeamsDataModel(
                    TeamsData.numberArray[i],
                    TeamsData.date[i],
                    TeamsData._id[i]
            ));
        }

        adapter = new TeamsAdapter(data, inflater);
        recyclerView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return fragmentTeamsSubLayout;
    }


}
