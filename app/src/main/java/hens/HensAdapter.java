package hens;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rouftom.can2019.R;

import java.util.ArrayList;

class HensAdapter extends RecyclerView.Adapter<HensAdapter.MyViewHolder> {
    private ArrayList<HensDataModel> dataSet;
    private LayoutInflater layoutInflater;
    static View self;
    private static AlertDialog customDialog;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView hensMatchDetails;
        TextView trType;
        TextView trAmount;
        TextView trProtagonistType;
        ImageView trIcon;
        TextView trDate;
        TextView trID;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.hensMatchDetails = (ImageView) itemView.findViewById(R.id.hens_match_details);
            self = itemView;
            /*this.trType = (TextView) itemView.findViewById(R.id.trType);
            this.trAmount = (TextView) itemView.findViewById(R.id.trAmount);
            this.trProtagonistType = (TextView) itemView.findViewById(R.id.trProtagonistType);
            this.trIcon = (ImageView) itemView.findViewById(R.id.trIcon);
            this.trDate = (TextView) itemView.findViewById(R.id.trDate);
            this.trID = (TextView) itemView.findViewById(R.id.trID);*/
        }
    }

    public HensAdapter(ArrayList<HensDataModel> data, LayoutInflater layoutInflater) {
        this.dataSet = data;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_hens_sub_line, parent, false);

        //view.setOnClickListener(AccountTransactions.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        ImageView hensMatchDetails = holder.hensMatchDetails;
        hensMatchDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

        /*TextView trType = holder.trType;
        TextView trAmount = holder.trAmount;
        TextView trProtagonistType = holder.trProtagonistType;
        TextView trDate = holder.trDate;
        TextView trID = holder.trID;
        ImageView trIcon = holder.trIcon;

        trType.setText(dataSet.get(listPosition).getType());
        trAmount.setText(dataSet.get(listPosition).getAmount());
        trProtagonistType.setText(dataSet.get(listPosition).getProtagonistType());
        trDate.setText(dataSet.get(listPosition).getDate());
        trID.setText(dataSet.get(listPosition).getID());
        trIcon.setImageResource(dataSet.get(listPosition).getImage());*/
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    void showCustomDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(self.getContext());
        View mView = layoutInflater.inflate(R.layout.dialog_match_details, null);
        /*TextView dialogMessage = (TextView) mView.findViewById(R.id.dialog_message);
        ImageView dialogImg = (ImageView) mView.findViewById(R.id.dialog_img);
        Button btnDialog = (Button) mView.findViewById(R.id.dialog_dismiss_btn);*/

        mBuilder.setView(mView);
        customDialog = mBuilder.create();
        customDialog.setCancelable(true);
        customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                customDialog = null;
            }
        });
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();
    }


}
