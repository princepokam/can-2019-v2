package hens;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import hens.HensFragmentSub;

public class HensViewAdapter extends FragmentPagerAdapter {
    // tab titles
    private String[] tabTitles = new String[]{"Poule A", "Poule B", "Poule C", "Poule D", "Poule E",
        "Poule F", "Poule G", "Poule H", "Poule I", "Poule J", "Poule K"};

    public HensViewAdapter(FragmentManager fm) {
        super(fm);
    }

    // overriding getPageTitle()
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                HensFragmentSub hensFragmentSub = new HensFragmentSub();
                return hensFragmentSub;

            case 1:
                HensFragmentSub hensFragmentSub1 = new HensFragmentSub();
                return hensFragmentSub1;

            case 2:
                HensFragmentSub hensFragmentSub2 = new HensFragmentSub();
                return hensFragmentSub2;

            case 3:
                HensFragmentSub hensFragmentSub3 = new HensFragmentSub();
                return hensFragmentSub3;

            case 4:
                HensFragmentSub hensFragmentSub4 = new HensFragmentSub();
                return hensFragmentSub4;

            case 5:
                HensFragmentSub hensFragmentSub5 = new HensFragmentSub();
                return hensFragmentSub5;

            case 6:
                HensFragmentSub hensFragmentSub6 = new HensFragmentSub();
                return hensFragmentSub6;

            case 7:
                HensFragmentSub hensFragmentSub7 = new HensFragmentSub();
                return hensFragmentSub7;
            case 8:
                HensFragmentSub hensFragmentSub8 = new HensFragmentSub();
                return hensFragmentSub8;
            case 9:
                HensFragmentSub hensFragmentSub9 = new HensFragmentSub();
                return hensFragmentSub9;
            case 10:
                HensFragmentSub hensFragmentSub10 = new HensFragmentSub();
                return hensFragmentSub10;
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}
