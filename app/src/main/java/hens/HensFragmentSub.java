package hens;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.rouftom.can2019.R;

import java.util.ArrayList;

import lib.LinearLayoutManagerWithSmoothScroller;

/**
 * A simple {@link Fragment} subclass.
 */
public class HensFragmentSub extends Fragment {
    ImageView tableViewToggle;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<HensDataModel> data;
    LinearLayout l1, ll;
    TextView txt;

    public HensFragmentSub() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View fragmentHensSubLayout = inflater.inflate(R.layout.fragment_hens_sub, container, false);
        tableViewToggle = (ImageView) fragmentHensSubLayout.findViewById(R.id.hens_table_toggle);
        recyclerView = (RecyclerView) fragmentHensSubLayout.findViewById(R.id.hens_list);
        l1 = (LinearLayout) fragmentHensSubLayout.findViewById(R.id.hens_layout_classement);
        txt = (TextView) fragmentHensSubLayout.findViewById(R.id.fragment_hens_sub_title);
        layoutManager = new LinearLayoutManager(getContext());

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManagerWithSmoothScroller(getContext()));
        txt.setText("Tour préliminaire (12/10/2018 - 05/03/2019");

        tableViewToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (l1.getVisibility() == View.VISIBLE) {
                    l1.setVisibility(View.GONE);
                    tableViewToggle.setImageResource(R.drawable.ic_visibility_off_black_24dp);
                } else {
                    l1.setVisibility(View.VISIBLE);
                    tableViewToggle.setImageResource(R.drawable.ic_visibility_purple_24dp);
                }
            }
        });

        data = new ArrayList<HensDataModel>();
        for (int i = 0; i < MyData.numberArray.length; i++) {
            data.add(new HensDataModel(
                    MyData.numberArray[i],
                    MyData.date[i],
                    MyData._id[i]
            ));
        }

        adapter = new HensAdapter(data, inflater);
        recyclerView.setAdapter(adapter);

        loadData();
        //new HensDataLoader().execute();
        // Inflate the layout for this fragment
        return fragmentHensSubLayout;
    }


    private void loadData() {
        LinearLayout lliv;
        ImageView imageView;
        TextView tv;

        try {
            int count = 1;
            while (count < 5) {
                // Create the table row
                ll = new LinearLayout(getContext());
                ll.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                ll.setOrientation(LinearLayout.HORIZONTAL);

                for (int i = 1; i < 10; i++) {
                    if (i != 2) {
                        tv = new TextView(getContext());
                        tv.setId(count*i+900);
                        tv.setTextColor(Color.parseColor("#757575"));
                        if (i >= 5) tv.setGravity(Gravity.CENTER);
                        if (i == 3) {
                            tv.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 4f));
                            tv.setText("Cameroun");

                        } else {
                            tv.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f));
                            tv.setText(String.valueOf(count));
                        }

                        //tv.setPadding(3, 3, 3, 3);
                        ll.addView(tv);
                        continue;
                    }

                    lliv = new LinearLayout(getContext());
                    lliv.setId(count*1500);
                    lliv.setLayoutParams(new LinearLayout.LayoutParams(
                            0,
                            LinearLayout.LayoutParams.WRAP_CONTENT, 1f));

                    imageView = new ImageView(getContext());
                    imageView.requestLayout();
                    imageView.setId(count+700);
                    imageView.setImageResource(R.drawable.cm);
                    imageView.setPadding(3, 3, 3, 3);
                    imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    imageView.setLayoutParams(new LinearLayout.LayoutParams(40, 40));
                    imageView.requestLayout();
                    lliv.addView(imageView);
                    ll.addView(lliv);
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(ll.getParent() != null)
                            ((ViewGroup)ll.getParent()).removeView(ll);

                        l1.addView(ll, new LinearLayout.LayoutParams(
                                TableLayout.LayoutParams.FILL_PARENT,
                                TableLayout.LayoutParams.WRAP_CONTENT));
                    }
                });

                count++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class HensDataLoader extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... values) {


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            /*(new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, 3000);*/
        }

    }



}
