package teams;

public class TeamsDataModel {
    String protagonistType;
    String date;
    String ID;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public TeamsDataModel(String protagonistType, String date, String id) {
        this.protagonistType = protagonistType;
        this.date = date;
        this.ID = id;
    }
}
