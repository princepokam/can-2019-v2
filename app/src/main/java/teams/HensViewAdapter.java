package teams;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class HensViewAdapter extends FragmentPagerAdapter {
    // tab titles
    private String[] tabTitles = new String[]{"Poule A", "Poule B", "Poule C", "Poule D", "Poule E",
        "Poule F", "Poule G", "Poule H", "Poule I", "Poule J", "Poule K"};

    public HensViewAdapter(FragmentManager fm) {
        super(fm);
    }

    // overriding getPageTitle()
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TeamsFragmentSub teamsFragmentSub = new TeamsFragmentSub();
                return teamsFragmentSub;

            case 1:
                TeamsFragmentSub teamsFragmentSub1 = new TeamsFragmentSub();
                return teamsFragmentSub1;

            case 2:
                TeamsFragmentSub teamsFragmentSub2 = new TeamsFragmentSub();
                return teamsFragmentSub2;

            case 3:
                TeamsFragmentSub teamsFragmentSub3 = new TeamsFragmentSub();
                return teamsFragmentSub3;

            case 4:
                TeamsFragmentSub teamsFragmentSub4 = new TeamsFragmentSub();
                return teamsFragmentSub4;

            case 5:
                TeamsFragmentSub teamsFragmentSub5 = new TeamsFragmentSub();
                return teamsFragmentSub5;

            case 6:
                TeamsFragmentSub teamsFragmentSub6 = new TeamsFragmentSub();
                return teamsFragmentSub6;

            case 7:
                TeamsFragmentSub teamsFragmentSub7 = new TeamsFragmentSub();
                return teamsFragmentSub7;
            case 8:
                TeamsFragmentSub teamsFragmentSub8 = new TeamsFragmentSub();
                return teamsFragmentSub8;
            case 9:
                TeamsFragmentSub teamsFragmentSub9 = new TeamsFragmentSub();
                return teamsFragmentSub9;
            case 10:
                TeamsFragmentSub teamsFragmentSub10 = new TeamsFragmentSub();
                return teamsFragmentSub10;
                default:
                    return null;
        }
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}
