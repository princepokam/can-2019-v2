package teams;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.rouftom.can2019.R;

import java.util.ArrayList;

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.MyViewHolder> {
    private ArrayList<TeamsDataModel> dataSet;
    private LayoutInflater layoutInflater;
    static View self;
    private static AlertDialog customDialog;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        RatingBar teamsRatingBar;
        ImageView teamsDetails;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.teamsRatingBar = (RatingBar) itemView.findViewById(R.id.teams_rating_bar);
            this.teamsDetails = (ImageView) itemView.findViewById(R.id.teams_details);
            self = itemView;
        }
    }

    public TeamsAdapter(ArrayList<TeamsDataModel> data, LayoutInflater layoutInflater) {
        this.dataSet = data;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_teams_sub_line, parent, false);

        //view.setOnClickListener(AccountTransactions.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        RatingBar teamsRatingBar = holder.teamsRatingBar;
        Drawable drawable = teamsRatingBar.getProgressDrawable();
        drawable.setColorFilter(Color.parseColor("#eeddaa"), PorterDuff.Mode.SRC_ATOP);

        ImageView teamsDetails = holder.teamsDetails;
        teamsDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

        //TextView trType = holder.trType;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    void showCustomDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(self.getContext());
        View mView = layoutInflater.inflate(R.layout.dialog_teams_details, null);
        //TextView dialogMessage = (TextView) mView.findViewById(R.id.dialog_message);

        mBuilder.setView(mView);
        customDialog = mBuilder.create();
        customDialog.setCancelable(true);
        customDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                customDialog = null;
            }
        });
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.show();
    }


}
