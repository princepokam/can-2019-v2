package teams;

public class TeamsData {

    public static String [] numberArray = {
            "645 85 47 14", "698 52 75 12", "233 58 41 75",
            "694 22 37 28", "694 10 25 47", "655 14 85 21",
            "699 99 99 99", "696 17 11 76", "644 44 44 44",
            "699 99 99 99", "699 99 99 99"
    };

    public static String[] date = {
            "sep 09 2018 12:14", "jui 26 2018 14:54", "jui 25 2018 08:14",
            "sep 09 2018 12:14", "jui 26 2018 14:54", "jui 25 2018 08:14",
            "sep 09 2018 12:14", "jui 26 2018 14:54", "jui 25 2018 08:14",
            "sep 09 2018 12:14", "jui 26 2018 14:54"
    };

    public static String[] _id = {
            "5b8dfc09eee43d42e7419dd3", "5b8dfc09eee43d42e7419dd4", "5b8dfc09eee43d42e7419dd5", "5b8dfc6deee43d42e7419dd9",
            "5b8dfc6deee43d42e7419dda", "5b8dfc6deee43d42e7419ddb", "5b8dfca8eee43d42e7419dde", "5b8dfca8eee43d42e7419ddf",
            "5b8dfca8eee43d42e7419de0", "5b8dfcd6eee43d42e7419de2", "5b8dfcd6eee43d42e7419de3"
    };
}
